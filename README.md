[![Release](https://gitlab.com/Xoores/snxwrapper/-/jobs/artifacts/master/raw/badges/version.svg?job=build_badges)]() [![Release](https://gitlab.com/Xoores/snxwrapper/-/jobs/artifacts/master/raw/badges/pylint.svg?job=build_badges)](https://gitlab.com/Xoores/snxwrapper/-/jobs/artifacts/master/raw/pylint.log?job=pylint_check)


snxwrapper is a tool created for effortless and automated connection to CheckPoint's SNX VPN.


* [ABOUT](#about)
* [FEATURES](#features)
    * [Automated login](#automated-login)
    * [ReAuth support](#reauth-support)
    * [Reconnect support](#reconnect-support)
    * [Config encryption](#config-encryption)
    * [Dispatcher script](#dispatcher-script)
    * [Fingerprint support](#fingerprint-support)
* [INSTALLATION](#installation)
    * [Dependencies](#python-dependencies)
    * [SNX Binary](#snx-binary)
* [USAGE AND OPTIONS](#usage-and-options)
* [CONFIGURATION FILE](#configuration-file)
	* [Configuration security and encryption](#configuration-security-and-encryption)
    * [Configuration file format](#configuration-file-format)
    * [Sample configuration file](#sample-configuration-file)
* [DISPATCHER](#dispatcher)
    * [Dispatcher ENV variables](#dispatcher-env-variables)
    * [Dispatcher actions](#dispatcher-actions)
    * [Sample dispatcher script](#sample-dispatcher-script)
* [ERROR REPORTING](#error-reporting)

# ABOUT
When connecting to SNX VPN you have to manually login in to the WEB VPN portal and install/run its CScript/Java companion app. You also have to re-authenticate every time your session is about to expire by clicking around the WEB VPN Portal which I find highly annoying and generally consider it to be a PITA.

I searched around and found some ancient wrapper tool that someone wrote to achieve automated connection but the tool was no longer maintained and did not really work. So I decided to completely recreate the functionality in Python3 with [AsyncIO](https://docs.python.org/3/library/asyncio.html). I also opted for no interactivity in this as I prefer to just use configuration file and store everything there.

I use original SNX binary that you can download from you CheckPoint WEB VPN Portal and my wrapper communicates with this binary using their proprietary binary protocol. I think that I've managed to figure out how this works but this is based on my research of available resources such as SNX binary, its Java companion and stuff like that. It is also possible that CheckPoint will change SNX or the protocol and this might stop suddenly working - keep this in mind, this comes with NO WARRANTY AT ALL and you are using it on your own risk.

I'm not a Python veteran by any means, so you might find this code to be ugly. Pull requests and good ideas are always welcome :-)

Also, please keep in mind that I wrote this for my use case, so I've implemented and tested just the things I tend to use. If you want to extend the functionality you can either fork this, create a PR or create a feature request issue but keep in mind that I work on this in my free time and might not be able to react in realtime to these.


# FEATURES

### Automated login
snxwrapper takes username and password and tries to log in to your CheckPoint's WEB VPN Portal. If successfull, it will try to gather the necessary information from the Portal that are required by SNX binary itself. It also supports saving HTTP session cookies, so it can re-authenticate faster without creating fresh session with each run

### ReAuth support
CheckPoint allows VPN sessions to be extended using Portal. In my case our timeout is set to 2h so if you want to stay connected, you have to go through ReAuth and extend the timer. If you don't do this, it will just cut your connection with "*User authentication expired*" error message and after reconnection you get a new IP address.

### Reconnect support
This wrapper will also try to keep the connection up. If it encounters an error that is known to be recoverable, it will wait for a while and then try again.

### Config encryption
I don't quite like the idea of my passwords laying around in plaintext config files. That's why I included possibility to encrypt certain config values using symmetric cipher (Salsa20). It is by no means bulletproof as the default key (if not specified) is derived from hostname/username. It is mainly intended to be used in a way that would allow you to edit the configuration file without having your password in plain sight. Since keyspace is quite small (16 bytes) it would also be quite easy to bruteforce this so **do not upload your config files to a public place!**

If you do want to share your config, please consider using `--dump-config` option that will display your config with all sensitive values removed.

If you want to encrypt config values, you cat run `--dump-config --sensitive`. This command will spit out your full configuration and suggest replacing plaintext values with encrypted ones.

You can also use **[libsecret](https://gitlab.gnome.org/GNOME/libsecret)** as a backend for storing sensitive information by just prepending `SECRET:` to any value in your config. I recommend using libsecret if you can as it is much more bulletproof solution.

### Dispatcher script
Since I'm an [i3](https://github.com/Airblader/i3) user, I tend to create my own automation of things such as notifications and updates of status bar etc. That's why I've included support for so-called "Dispatcher script".

Dispatcher script is an executable that snxwrapper will call as it goes through various phases of VPN connection process. You can then implement actions to be triggered by any event such as VPN up, VPN down etc.

Dispatcher inherits your ENV so you can use `notify-send` and similar (DBUS-related things).

Sample dispatcher script is [here](#dispatcher).

### Fingerprint support
SNX will validate your server's SSL fingerprint which is represented by a series of short english words - something like `FLEW LADY SOIL ADEN HIM CANT`.

By default, snx will try to connect to the VPN and if it has not yet seen your server, it will try to ask you to verify its fingerprint. That is, of course, quite OK because your admin *should* tell you what fingerprint to expect and by checking this you can be sure that you are talking to your server.

The problem is that (a) snx will not tell snxwrapper that it is waiting for such confirmation so snxwrapper will eventually kill snx and try again repeating this whole thing again and (b) snx uses X11 popup to show the question.
The latter is a huge pain since you might not have X.11 at all (hello Wayland) meaning that you don't get *any* notification at all and snx will just silently fail leaving you wondering WTF. Or if you have HiDPI screen/multi-monitor setup, you will get to play "Where is Waldo" searching for a teeny X.11 window.

snxwrapper allows you to skip this pain by using [fingerprint](#fingerprint) in your configuration, however you will have to ensure that your user has write permissions to its trustdb file in /etc/snx - more details on that in [fingerprint](#fingerprint) section of config.

snxwrapper will also check fingerprint (either from your config file or trustdb) against your server and will refuse to connect if there is mismatch.

# INSTALLATION
Installation is quite straightforward - this whole thing is written as a single file that can be placed anywhere you want.

### Gentoo
For Gentoo you can use ebuilds from [my overlay](https://gitlab.com/Xoores/gentoo-overlay/-/tree/main/net-vpn/snxwrapper) which will pull all necessary Python dependencies.

### Ubuntu (or other Debian-based distros)
For Ubuntu/Debian-based distros you should probably use apt to install the necessary Python dependencies instead of PIP.

You can do it with this command: 

     sudo apt-get install python3-aiohttp python3-async-timeout python3-lxml python3-aioopenssl 
     python3-openssl python3-pycryptodome python3-rsa python3-bs4

### Python Dependencies
snxwrapper depends on following Python libraries:
* aiohttp
* aiosignal
* BeautifulSoup4
* pycryptodome
* lxml
* PyOpenSSL

You should be good to go using provided requirements.txt file by running something like: `pip3 install -r requirements.txt`

This should pull all the necessary packages.

### SNX binary
You will also need the original SNX binary from CheckPoint. It can be downloaded either from 
 * [CheckPoint's website](https://supportcenter.checkpoint.com/supportcenter/portal/user/anon/page/default.psml/media-type/html?action=portlets.DCFileAction&eventSubmit_doGetdcdetails=&fileid=22824) or 
 * from your VPN SLL portal at `https://YOUR-CHECKPOINT-ADDRESS/SNX/INSTALL/snx_install.sh`.

This file as far as I know does contain only the SNX and the uninstaller, but you can of course get those files out and copy just the binary.

You can also use `--download-snx` option, that will download and extract snx binary directly from your CheckPoint firewall and extract it from installation script to /tmp. Advantage of this is that you will always get the binary that your CheckPoint expect you to use. Just run `snxwrapper --download-snx`.

ℹ️ You will have to install the downloaded snx binary somewhere usefull on your system. You can achieve this by running this command:

     sudo sh -c 'mv /tmp/snx /usr/bin/snx && chown root:root /usr/bin/snx && chmod 4755 /usr/bin/snx'

I also added version check of the SNX, so if you are not using the recommended version, you will get a warning. I did not yet run into any problems using older version of SNX.

⚠️ Also **SNX binary is 32 bit** - at least the version I'm currently using (*build 800008304*) is:

    $ file /usr/bin/snx
    /usr/bin/snx: setuid ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked,
    interpreter /lib/ld-linux.so.2, for GNU/Linux 2.2.5, stripped

So if you have no multilib support, you will have to install all the necessary bits and pieces to make it work.

⚠️ Recently I found out that on 🐧Linux **SNX binary does not support ECDSA certificates** (at least the versions I tried, the latest I can get right now is *build 800008304*)


# USAGE AND OPTIONS
	usage: snxwrapper [-h] [-v] [--download-snx] [--clear-cookies] [-d] [--dump-config] [--sensitive]
                  [-c CONFIG] [-k KEY]
                  [server]
     
	positional arguments:
	  server                Target server specified in configuration
     
	optional arguments:
	  -h, --help            show this help message and exit
	  --version, -v         Show version
      --download-snx        Download SNX from your CheckPoint
	  --clear-cookies       Clears saved cookies and forces login with username/password for the first run
     
	Logging options:
	  -d, --debug           Enable debugging messages
     
	Config-related options:
	  --dump-config         Dump ANONYMIZED config and exit
	  --sensitive           with --dump-config shows full config as is
	  -c CONFIG, --config CONFIG
	                        Configuration file
	  -k KEY, --key KEY     Config file encryption key - using symmetric Salsa20 cipher and the default key
	                        is derived from hostname/username. This is mainly used for obfuscation in case
	                        that you manage show your config to someone etc. THIS IS NOT BULLETPROOF!


# CONFIGURATION FILE
By default, snxwrapper tries to load configuration from `~/.config/snxwrapper/config` but you can override this with `--config` option


> [!WARNING] 
> **CheckPoint firewall has a feature that temporarily blacklists you for a period of time if you try & fail login multiple times in a row.**
> Inf you have an error in your config file (misspelled username, wrong certificate etc) and you suddenly get errors like `Failed to download /Portal/Main` over and over, stop the snxwrapper and just wait for a while!

### Configuration security and encryption

You can (and frankly should) encrypt atleast your password in snxwrapper config. You can either use integrated encryption or libsecret (recommended). You can use libsecret by adding your sensitive information to libsecret storate using either CLI interface such as `secret-tool` or GUI tool such as [Seahorse](https://wiki.gnome.org/Apps/Seahorse/).

Snxwrapper searches stored data using ID. You can add your password to your libsecret storage using this command:

    secret-tool store --label="Workplace password" id WORK-bob

This command will store your password under id `WORK-bob` and you can use it by just adding it to your config using `SECRET:` prefix like this:

    password = SECRET:WORK-bob

You can store any configuration value like this.

🔑 Remember to stay safe and always encrypt!


### Configuration file format
snxwrapper implements basic configuration language which provides a structure similar to what’s found in Microsoft Windows INI files using Python's [configParser](https://docs.python.org/3/library/configparser.html) library If you are interested in more details.

Let me quote from configparser documentation about formatting:
A configuration file consists of sections, each led by a `[section]` header, followed by key/value entries separated by an equals sign (`=`). Section names are case-sensitive but keys are not. Leading and trailing whitespace is removed from keys and values. Values can NOT be omitted. Values can also span multiple lines, as long as they are indented deeper than the first line of the value.

Configuration files may include comments, prefixed by a hash character (`#`). Comments may appear on their own on an otherwise empty line, possibly indented. Inline comments are NOT allowed.

Any unknown configuration variables will be silently ignored as long as the config file format is valid.


Configuration file consist of 3 section types:
#### Section [global]
Global section is used for specifying options that should be applied to all specified host. These can, of course, be overriden in each section specifically.

#### Section [snxwrapper]
This section configures snxwrapper itself (global options).

Possible keys:
##### snx-path
*Default=/usr/bin/snx*

##### save-cookies
*Default=true*

Enable/disable caching cookies. I think it is better enabled as it speeds up logging in when re-connecting, also preventing creating multiple SSL VPN sessions. When you disable this, you may easily exceed your licences on your firewall resulting in DoS until those orphan session time out...

Cookies are saved by default in `~/.cache/snxwrapper.cookies`

##### default-connection
*Default=None*

Default Host (host section) to be used if no host is specified as CLI parameter. Allowing you to run snxwrapper without any visible parameters.

##### auto-reconnect
*Default=True*

##### dispatcher-path
*Default=None*

Path to your [Dispatcher script](#dispatcher).

##### debug
*Default=False*

Option that enables debug by default without need to specify `-d` all the time

##### syslog
*Default=True*

##### censor-logs
*Default=True*

Tries to hide any sensitive or user identifying information in logs. It is still advised to
check that I did not miss anything.

##### run-as-root
*Default=False*

Allows snxwrapper to run as root. By default snxwrapper will refuse to run as root, because it is not necessary and it also means that any bug/problem in snxwrapper could affect security of your system.

Be aware that snxwrapper uses some external libraries and also downloads & parses data from unknown sources (your CheckPoint firewall or something posing as your firewall). That means that if there is any remote code execution bug or similar security vulnerability in any code used by snxwrapper (not only snxwrapper itself), it may gain full privileges on your system.

If you really need to do some privileged work, use [Dispatcher scripts](#dispatcher) instead - you can easily bootstrap these using `sudo` to handle work as root.

If you think you need to run snxwrapper as root, you can - but please be safe and try to avoid it as possible. Due to the security implications of this decision, I decided to put this override to the config file only. 

##### dump-all-http
*Default=False*

Save all downloaded HTTP pages from VPN portal to `/tmp` using following template:  `/tmp/.snx.$TIMESTAMP.$PAGE`. Useful for debugging why something is not working as expected.

##### user-agent
*Default="Mozilla/5.0 Chrome/105.0.0.0"*

Since CheckPoint now checks User-Agent, we are spoofing Chrome by default. You can specify whatever you like if the default stops working in future upgrades.

#### Host sections
This type of section describes specific host or alias.

If you have only one Host section, it is considered as the default one, and you don't have to
specify `server` on CLI nor `default-connection` in config

##### host
*Default=(section name)*

You can either name your section with hostname or you can specify it here

##### username
*Default=None* **[MANDATORY]**

##### password
*Default=None* **[MANDATORY]**

##### fingerprint
*Default=None*

Allows you to configure server fingerprint (see [Fingerprint support](#fingerprint-support)). Possible values are either `TOFU` or fingerprint string.

If you specify `TOFU`, snxwrapper will get fingerprints from your server and save them to your trustdb file.

If you specify fingerprint string directly, snxwrapper will put that into your trustdb file.

ℹ️ You have to have permissions to write to your trustdb file (by default `/etc/snx/${USER}.db`) for this feature to work. You can achieve this by running this command:

     sudo sh -c 'chmod ga+rx /etc/snx && sudo touch /etc/snx/$(id -un).db && sudo chown $(id -u) /etc/snx/$(id -un).db && sudo chmod 600 /etc/snx/$(id -un).db'

This will do 3 things:
1. Allow everyone to read and enter `/etc/snx` 
1. Create file in `/etc/snx` with your user's name if it does not exist
1. Change ownership of this file to you
1. Set permissions so only you and root can read the file

##### realm
*Default=ssl_vpn*

##### login_type
*Default=Standard*

##### ca-certificate
*Default=None*

If you use self-signed certificate for your firewall, you should set this. Otherwise, you will likely run into a SSL verification error.

##### ssl-no-verify
*Default=False*

I **strongly** suggest that you use this only for debugging purposes as this effectively opens up your VPN to MITM attacks.

Also remember, that there is a special place in hell for people that keep this set to True long term!

### Sample configuration file
```
[snxwrapper]
snx-path = /usr/bin/snx
dispatcher = /scripts/cp_dispatcher.sh


[vpn.example.com]
username = hardworker
password = T0tallyS#curePassword,eh?
ca-certificate = 
	-----BEGIN CERTIFICATE-----
	MIIDdDCCAlygAwIBAgIEHS4ZkDANBgkqhkiG9w0BAQsFADBTMQswCQYDVQQGEwJK
	...
	adKKQtDNYsKHViSx87i3OFK7SUHBqCKv
	-----END CERTIFICATE-----
```

You would connect to `vpn.example.com` using following command: `snxwrapper vpn.example.com` or just `snxwrapper` since there is only one defined host, and therefore it is considered as the default


# DISPATCHER
This section describes dispatcher scripts.

Basic idea is that user defined script is called every time snxwrapper state changes (called [Dispatcher action](#dispatcher-actions)). This script is provided with information about this state change using ENV variables.

### Dispatcher ENV variables
##### SNX_ACTION
Present: always

Known [Dispatcher actions](#dispatcher-actions)

##### SNX_SERVER
Present: always

Selected host. Basically [host section](#host-sections) from configuration file.

##### SNX_FAILED_REASON
Present: VPN_DOWN

Reason for disconnection that SNX reported in DISCONNECTED_RP message.

### Dispatcher actions
Following dispatcher actions are implemented at the moment. Please keep in mind that these can change since this is just a first release. I will also extend this section later with some more details about these actions.

Basic flowchart is as follows:
```mermaid
graph TD
    A[PRE_LOGIN] -->|Load config, decrypt...| B[LOAD_CONFIG]
    B --> C[PRE_LOGIN]
    C --> D{Try to log in}
    D -->|Failure| LFAIL[LOGIN_FAILED]
    D -->|Success| LOK[LOGIN_SUCCESS]
    LOK --> F[SNX_START]
    F -->|Prepare SNX payload, start snx| G[VPN_CONNECTING]
    G -->VPNCNX{Connected?}
    VPNCNX --> VPNDOWN[VPN_DOWN]
    VPNDOWN --> VPNERR{Recoverable error?}
    VPNERR -->|Recoverable| WAIT[WAIT_RECONNECT]
    VPNERR -->|Not recoverable| ZZ
    WAIT -->|After timeout| F
    VPNCNX --> VPNUP[VPN_UP]
    VPNUP --> WORK(Do work...)
    WORK -->|Problem| VPNERR
    WORK -->|Interrupt| ZZ
    B -->|Invalid config| ZZ[APP_END]
    LFAIL --> ZZ
```

##### LOAD_CONFIG
Fired when configuration is successfully parsed and there are no obvious errors.

##### PRE_LOGIN
Before login is tried

##### LOGIN_SUCCESS
##### LOGIN_FAILED
##### SNX_START
##### VPN_CONNECTING
##### VPN_UP
##### VPN_DOWN
##### WAIT_RECONNECT
##### APP_END
snxwrapper is quitting

### Sample Dispatcher script
```bash
#!/bin/bash

ACTION="${SNX_ACTION}"
SERVER="${SNX_SERVER}"

function log_unhandled()
{
	logger -t "snxdispatcher" -p7 "Unhandled event  [ACTION=${ACTION}  SERVER=${SERVER}]"
}


case "${ACTION}" in
	LOAD_CONFIG|\
	PRE_LOGIN|\
	LOGIN_SUCCESS|\
	LOGIN_FAILED|\
	SNX_START|\
	VPN_CONNECTING|\
	WAIT_RECONNECT)
		notification-send "VPN connecting" "State = ${ACTION}"
		;;
		
	VPN_DOWN)
		notification-send "VPN Disconnected"
		;;
	
	VPN_UP)
		notification-send "VPN Connected"
		;;

	*) log_unhandled ;;
esac
```

# ERROR REPORTING
Please keep in mind that even though I try to test this app, I have only limited options to do that since I only have one CheckPoint VPN available to me. My VPN has username/password login and default realm. That means that I never have a chance to test e.g. PIN login, VPN applications or many of the other features that CheckPoint VPN offers. I only use plain and simple VPN.

I also don't really have to deal with some checks/mandatory application policies etc. so YMMV.

If you want some advanced feature that I don't have access to, or you encounter an error, you can:
1. Create an issue here in hopes that I get around and implement said feature
1. Try to implement it yourself (and create merge request if possible so others can benefit from your suffering as well)
1. Use official CheckPoint GUI client

I will try to keep this thing updated and working as there are few people in my circles that use this as well, but I cannot guarantee anything. If you really want to help me narrow down an issue, please try to include as much information as you can.

You will have to likely provide me with full debug log (`-d` or `debug = True`) and possible other info as well. This might be obvious, but just to put it out here: **never ever include your full config with username/password!** 

snxwrapper has to try and parse HTTP pages from SSL portal and these may vary greatly. If snxwrapper runs into some weird issue in parsing these pages, it will dump that page to /tmp. Template for naming these dumps is `/tmp/.snx.$TIMESTAMP.$TARGET`. It will also complain in logs and tell you it created this file. It might be necessary for me to look at these files in order to fix regexp/parsing issues so please be prepared for that eventuality. 

Please keep in mind that there might be some pieces of identifying information present such as hostname of your VPN, company name etc. even if you have `censor-logs` enabled. I don't think that it would compromise your network in any way, however YMMV.
