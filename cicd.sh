#!/bin/bash

export PATH="/home/gitlab-runner/.local/bin/:${PATH}"

case "${1}" in
  prepare)
    echo "Running for branch ${CI_COMMIT_REF_NAME}"
    pip3 install anybadge pylint
    pip3 install -r requirements.txt
    ;;

  badges)
    echo "Running BADGES action"

    APP_VER="$(grep -e "^__version__" snxwrapper | cut -d'"' -f2)"
    PYLINT_SCORE="$(tail -n2 pylint.log | head -n1 | awk '{print $7}')"

    echo " |- APP_VER = ${APP_VER}"
    echo " |- PYLINT_SCORE = ${PYLINT_SCORE}"

    mkdir -v badges/

    anybadge --value="v${APP_VER}" --label="Version" --color=green --file=badges/version.svg
    anybadge --value="${PYLINT_SCORE}" --label="PyLint" --color=yellow --file=badges/pylint.svg
    ;;

  pylint)
    # C0103: Variable name XY doesn't conform to snake_case naming style (invalid-name)
    # C0302: Too many lines in module (1317/1000) (too-many-lines)
    # E0401: Unable to import 'aiohttp' (import-error)
    pylint --max-line-length=130 --output=pylint.log --fail-under=9 --disable=C0103,C0302,E0401 snxwrapper
    cat pylint.log
    ;;

  *)
    echo "Unknown action: ${1}"
    exit 1
    ;;

esac
