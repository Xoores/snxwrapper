# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [0.1.5] - 2024-11-22
### Added
 - HTTP downloads now have 3 tries so being on bad wifi connection does not suck that much
 - pylint.log to .gitignore

### Changed
 - Some small linter fixes

## [0.1.4] - 2023-02-17
### Added
- Some ENV debugging variables for SNX
- Version check of SNX against what CheckPoints says should be used
- CLI option `--download-snx` that allows you to get SNX directly from your firewall
- Possibility to use [libsecret](https://gitlab.gnome.org/GNOME/libsecret) to encrypt sensitive data
- Config option to allow running snxwrapper as ROOT (`run-as-root`)

### Fixed
- Missing %d from issue [#2](https://gitlab.com/Xoores/snxwrapper/-/issues/2)
- Syslog logging was turning on even when in TTY and without explicite use-syslog=True

### Changed
- snx debugs are now turned on only when `debug` is enabled in config
- cleaned up PyLint problems
- Removed TODO regarding moving timeouts to config and just added some description in comments
- Assorted small style changes/fixes
- Updated README.md to reflect new CLI option `--download-snx`
- Fixed some typos/strange wording in README.md
- snxwrapper will refuse to run as root by default. This can be overriden by config (`run-as-root`)

## [0.1.3] - 2022-09-25
### Added
- Possibility to manually define User-Agent in config file (`user-agent`)
- Proper handling of SIGTERM

### Fixed
- CheckPoint now checks UserAgent (FW version >= R81.10)
- TimeoutError in issue [#1](https://gitlab.com/Xoores/snxwrapper/-/issues/1)
- Few uninitialized variables in exception handling path
- Better logging, debugs & error handling in get_chain_fingerprints()

### Changed
- HTTP timeout is now fast-recoverable error (wait time 3s instead of 15s)

## [0.1.2]
### Added
- Possibility to manually define server fingerprint in config file (`fingerprint`)
- If `fingerprint` is configured, snxwrapper will try to create trustdb in /etc/snx
- Dispatcher VPN_UP now receives extra info (IP, domain, DNS servers)
- Option to dump all downloaded webpages (`dump-all-http`)

### Fixed
- Curly brackets around some variables after logging system rework
- Extra info should now be passed to Dispatcher script
- Missing libraries in PyLint check by installing requirements.txt

### Changed
- Minor tweaks in README.md
- Dispatcher script keeps ENV
- Reversed order of changelog so the newest version is at the top

## [0.1.1] - 2022-01-18
### Added
- Changelog :-)
- Many new options in config file (`host`, `syslog`, `debug` and more)
- Default connection logic
- Option `censor-logs` to remove sensitive info in logs (enabled by default)
- Logout support

### Changed
- Renamed `dispatcher` to `disaptcher-path` in config
- Target hostname now can be specified as `host` so you can alias Host sections
- Cleaned up logging (shortened long lines etc.)

### Removed
- CLI option `--syslog` as it was moved to config

### Fixed
- snxwrapper.save-cookies default is True
- snxwrapper.auto-reconnect default is True
- Missing return in main()
- SNX_TCP_Client.on_disconnect had unexpected parameter


## [0.1.0] - 2022-01-13
### Added
- First release and upload to git